# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QWidget
)
from PyQt5.QtGui import (
    QPainter,
    QColor,
    QFontMetrics
)
from PyQt5.QtCore import Qt


class LineNumberArea(QWidget):

    def __init__(self, editor):
        super(LineNumberArea, self).__init__(editor)
        self._editor = editor

    def paintEvent(self, event):
        painter = QPainter(self)
        painter.fillRect(event.rect(), QColor("#fafafa"))
        block = self._editor.firstVisibleBlock()
        block_number = block.blockNumber()
        current_block = self._editor.document().findBlock(
            self._editor.textCursor().position())
        top = self._editor.blockBoundingGeometry(block).translated(
            self._editor.contentOffset()).top()
        bottom = top + self._editor.blockBoundingRect(block).height()
        painter.setFont(self._editor.document().defaultFont())
        painter.setPen(QColor("lightGray"))

        while block.isValid() and top <= event.rect().bottom():
            if block.isVisible() and bottom >= event.rect().top():
                text = '-'
                line = block_number + 1
                if line % 10 == 0 or current_block == block or line == 1:
                    text = str(line)

                # Línea actual en negrita
                bold = False
                if current_block == block:
                    bold = True
                    font = painter.font()
                    font.setBold(True)
                    painter.setFont(font)

                painter.drawText(0, top, self.width(),
                                 self._editor.fontMetrics().height(),
                                 Qt.AlignHCenter, text)

                if bold:
                    font = painter.font()
                    font.setBold(False)
                    painter.setFont(font)

            block = block.next()
            top = bottom
            bottom = top + self._editor.blockBoundingRect(block).height()
            block_number += 1

        QWidget.paintEvent(self, event)

    def update_area(self):
        # Length of number
        digits = len(str(self._editor.blockCount()))
        # Width of char + magic number
        width = QFontMetrics(
            self._editor.document().defaultFont()).width('0' * digits) + 8
        if self.width() != width:
            self.setFixedWidth(width)
            # Adjuts viewport margins
            self._editor.setViewportMargins(width, 0, 0, 0)
        self.update()