# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

# Based on # http://diotavelli.net/PyQtWiki/Python%20syntax%20highlighting

import json

from PyQt5.QtGui import (
    QSyntaxHighlighter,
    QTextCharFormat,
    QColor
)

from PyQt5.QtCore import (
    QDir,
    QThread,
    QRegExp
)

from src import SYNTAX_FILES


SYNTAX_MAP = {
    'py': 'python'
}


class Syntax(object):

    def __init__(self):
        self.keywords = []
        self.extras = []
        self.string = []
        self.comment = None
        self.object = None


class Highlighter(QSyntaxHighlighter):
    """ Syntax Highlighter for Python, Ruby and Perl """

    def __init__(self, editor, syntax=None):
        super(Highlighter, self).__init__(editor)
        self.setDocument(editor.document())
        if syntax is not None:
            self.highlight(syntax)

    def highlightBlock(self, text):
        #self._thread.start_highlight(text)
        for expression, nth, format_ in self.rules:
            index = expression.indexIn(text, 0)

            while index >= 0:
                index = expression.pos(nth)
                length = len(expression.cap(nth))
                self.setFormat(index, length, format_)
                index = expression.indexIn(text, index + length)

        self.setCurrentBlockState(0)

        # Do multi-line strings
        in_multiline = self.match_multiline(text, *self.tri_single)
        if not in_multiline:
            in_multiline = self.match_multiline(text, *self.tri_double)

    def highlight(self, syntax):
        # Multi-line strings (expression, flag, style)
        # FIXME: The triple-quotes in these two lines will mess up the
        # syntax highlighting from this point onward
        self.tri_single = (QRegExp("'''"), 1, self.build_format("#50a14f"))
        self.tri_double = (QRegExp('"""'), 2, self.build_format("#50a14f"))

        rules = []

        # Keywords
        kwords = syntax.keywords
        pat = r'(^|[^\w\.]{1})(%s)([^\w]{1}|$)'
        rules += [(pat % kw, 2,
                  self.build_format('#b449b2')) for kw in kwords]
        # Extras
        extras = syntax.extras
        rules += [(pat % ex, 2,
                  self.build_format('#e45649')) for ex in extras]

        # Object, Python: self
        object_ = syntax.object
        rules += [(r'\b{}\b'.format(object_[0]),
                  0, self.build_format('#e45649', 'italic'))]

        #FIXME: for other langs
        # def and class followed by an identifier
        pattern = r'\b%s\b\s*(\w+)'
        rules += [(pattern % 'def', 1, self.build_format('#c18401'))]
        rules += [(pattern % 'class', 1, self.build_format('#c18401'))]

        # Numeric literals
        rules += [(r'\b[+-]?[0-9]+[lL]?\b', 0,
         self.build_format('#986801')),
        (r'\b[+-]?0[xX][0-9A-Fa-f]+[lL]?\b', 0,
         self.build_format('#986801')),
        (r'\b[+-]?[0-9]+(?:\.[0-9]+)?(?:[eE][+-]?[0-9]+)?\b', 0,
         self.build_format('#986801'))
        ]

        #FIXME: for other langs
        comment = syntax.comment
        rules += [(r'{}[^\n]*'.format(comment),
                  0, self.build_format('#a0a1a7'))]

        # Strings
        string = syntax.string
        for st in string:
            if st == "'":
                expression = r"'[^'\\]*(\\.[^'\\]*)*'"
            else:
                expression = r'"[^"\\]*(\\.[^"\\]*)*"'
            rules.append((expression, 0, self.build_format('#50a14f')))

        self.rules = [(QRegExp(pattern), index, format_)
                      for (pattern, index, format_) in rules]

        # Thread
        #self._thread = ThreadHighlight(self)

    def match_multiline(self, text, delimiter, in_state, style):
        """
        Do highlighting of multi-line strings. ``delimiter`` should be a
        ``QRegExp`` for triple-single-quotes or triple-double-quotes, and
        ``in_state`` should be a unique integer to represent the corresponding
        state changes when inside those strings. Returns True if we're still
        inside a multi-line string when this function is finished.
        """

        # If inside triple-single quotes, start at 0
        if self.previousBlockState() == in_state:
            start = 0
            add = 0
        # Otherwise, look for the delimiter on this line
        else:
            start = delimiter.indexIn(text)
            # Move past this match
            add = delimiter.matchedLength()

        # As long as there's a delimiter match on this line...
        while start >= 0:
            # Look for the ending delimiter
            end = delimiter.indexIn(text, start + add)
            # Ending delimiter on this line?
            if end >= add:
                length = end - start + add + delimiter.matchedLength()
                self.setCurrentBlockState(0)
            # No; multi-line string
            else:
                self.setCurrentBlockState(in_state)
                length = len(text) - start + add

            # Apply formatting
            self.setFormat(start, length, style)
            # Look for the next match
            start = delimiter.indexIn(text, start + length)

        # Return True if still inside a multi-line string, False otherwise
        if self.currentBlockState() == in_state:
            return True
        else:
            return False

    @staticmethod
    def build_format(color, style=''):
        """ Return a QTextCharFormat with the given attributes """

        format_ = QTextCharFormat()
        format_.setForeground(QColor(color))
        if 'italic' in style:
            format_.setFontItalic(True)
        return format_


class ThreadHighlight(QThread):

    def __init__(self, highlighter):
        super(ThreadHighlight, self).__init__()
        self._highlighter = highlighter

    def run(self):
        for expression, nth, format_ in self._highlighter.rules:
            index = expression.indexIn(self._text, 0)
            while index >= 0:
                index = expression.pos(nth)
                length = len(expression.cap(nth))
                self._highlighter.setFormat(index, length, format_)
                index = expression.indexIn(self._text, index + length)

        #self._highlighter.setCurrentBlockState(0)

    def start_highlight(self, text):
        self._text = text
        self.start()


def load_syntax(extension):
    """ Load syntax by extension i.e. py """

    lang = SYNTAX_MAP.get(extension, '')
    if not lang:
        return None
    lang += '.json'
    fname = QDir(SYNTAX_FILES).filePath(lang)
    with open(fname) as json_file:
        data = json.load(json_file)
    syntax = Syntax()
    syntax.keywords = data.get('keywords', [])
    syntax.extras = data.get('extras', [])
    syntax.string = data.get('string', [])
    syntax.comment = data.get('comment')
    syntax.object = data.get('object')

    return syntax
