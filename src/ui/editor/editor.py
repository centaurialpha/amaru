# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QPlainTextEdit,
    QTextEdit
)
from PyQt5.QtGui import (
    QPainter,
    QPen,
    QFontMetrics,
    QTextFormat,
    QColor,
    QTextCursor,
    QTextOption
)
from PyQt5.QtCore import (
    Qt,
    pyqtSignal,
    #QVarLengthArray,
    #QLineF
    #QLine,
    #QRect
)

from src.ui.editor import (
    #highlighter,
    line_number_widget,
    minimap
)

from src.ui.editor.syntax_highlighter import syntax_highlighter

from src.core.settings import Settings
from src.core import file_manager


def create_new_editor(object_file):
    editor = Editor(object_file)
    return editor


class Editor(QPlainTextEdit):
    """
    Editor Class

    The most important part of the application. This editor is a subclass of
    QPlainTextEdit, improving and adding other features such as
    Syntax Highlighter, Code Folding, custom Sidebar, Margin Line, Minimap,
    Bracket Matching, Autoindent, and more.
    """

    focusChanged = pyqtSignal('PyQt_PyObject')
    # Emit line, column, length and total lines
    cursorPositionChange = pyqtSignal(int, int, int, int)
    # Brackets
    OPEN_BRACKETS = '({['

    CLOSE_BRACKETS = ')}]'

    MAP_BRACKETS = dict((bracket, comp)
                   for (bracket, comp) in zip(OPEN_BRACKETS, CLOSE_BRACKETS))

    def config(self):
        document = self.document()
        option = QTextOption()

        if Settings.WORD_WRAP:
            self.setWordWrapMode(QTextOption.WrapAtWordBoundaryOrAnywhere)
        else:
            self.setWordWrapMode(QTextOption.NoWrap)
        if Settings.SHOW_TABS_AND_SPACES:
            option.setFlags(QTextOption.ShowTabsAndSpaces)

        document.setDefaultTextOption(option)
        self.setDocument(document)

    def __init__(self, object_file):
        super(Editor, self).__init__()
        # Configuration
        self.config()

        # Cursor width
        self.setCursorWidth(Settings.CURSOR_WIDTH)

        # Style sheet
        self.set_editor_colors()

        self.fobject = object_file
        # Minimap
        self._minimap = None
        if Settings.MINIMAP:
            self._minimap = minimap.Minimap(self)
            self.updateRequest.connect(self._minimap.update_area)

        # Load Font
        #FIXME: hacer método
        self.document().setDefaultFont(Settings.FONT)

        # Highlighter
        self._highlighter = None
        ext = file_manager.get_extension(self.fobject.filename)
        parts_scanner, code_scanner, fmts = syntax_highlighter.load_syntax(ext)
        if parts_scanner is not None:
            self._highlighter = syntax_highlighter.SyntaxHighlighter(
                self.document(), parts_scanner, code_scanner, fmts,
                self.document().defaultFont())
        # Números de línea
        self._line_number_widget = None
        if Settings.SHOW_LINE_NUMBERS:
            self._line_number_widget = line_number_widget.LineNumberArea(self)

        # Indentation
        self.set_indentation_width(4)

        # The toPlainText method takes a long time on large files,
        # so it is cached
        self._cache = None

        # Right margin line position
        self.margin_position = Settings.MARGIN_LINE_WIDTH * 10

        self._brace_position = None
        # Connections
        if self._line_number_widget is not None:
            self.updateRequest.connect(
                self._line_number_widget.update_area)
        self.cursorPositionChanged.connect(self._highlight_current_line)
        self.textChanged.connect(self._reset_cache)

        self._highlight_current_line()

    def _reset_cache(self):
        self._cache = None

    def set_editor_colors(self):
        background = "#fafafa"
        color = "gray"
        style = "QPlainTextEdit {color: %s; border: none; background: %s}" \
                 % (color, background)
        self.setStyleSheet(style)

    def set_indentation_width(self, iw):
        Settings.INDENTATION_WIDTH = iw
        iw *= 10
        self.setTabStopWidth(iw)

    @property
    def modified(self):
        return self.document().isModified()

    @modified.setter
    def modified(self, m):
        self.document().setModified(m)

    @property
    def text(self):
        if self._cache is None:
            self._cache = QPlainTextEdit.toPlainText(self)
        return self._cache

    @text.setter
    def text(self, txt):
        QPlainTextEdit.setPlainText(self, txt)

    def paintEvent(self, event):
        """ Draw the margin line  """

        QPlainTextEdit.paintEvent(self, event)
        if Settings.MARGIN_LINE:
            painter = QPainter()
            painter.begin(self.viewport())
            painter.setPen(QColor("lightGray"))
            painter.drawLine(self.margin_position, self.height(),
                             self.margin_position, 0)

        painter.setPen(Qt.NoPen)
        color = QColor("lightGray")
        #TODO: configurable
        color.setAlpha(50)
        painter.fillRect(self.margin_position, -1,
                         self.width() - self.margin_position, self.height(),
                         color)
        painter.end()

    def focusInEvent(self, event):
        super(Editor, self).focusInEvent(event)
        self.focusChanged.emit(self)

    def resizeEvent(self, event):
        # Ajusto la altura del widget
        # Widget números de línea
        if self._line_number_widget is not None:
            self._line_number_widget.setFixedHeight(self.height())
        # Minimapa
        if self._minimap is not None:
            self._minimap.resize()

    def show_line_numbers(self):
        val = True
        if Settings.SHOW_LINE_NUMBERS:
            val = False
            self._line_number_widget.enable = False
            self._line_number_widget = None
        else:
            self._line_number_widget = line_number_widget.LineNumberArea(self)
        Settings.SHOW_LINE_NUMBERS = val

    def _highlight_current_line(self):
        extra_selections = []

        selection = QTextEdit.ExtraSelection()
        line_color = QColor("lightGray").lighter(117)
        selection.format.setBackground(line_color)
        selection.format.setProperty(QTextFormat.FullWidthSelection, True)
        selection.cursor = self.textCursor()
        selection.cursor.clearSelection()
        extra_selections.append(selection)
        self.setExtraSelections(extra_selections)

        # Emit cursor position changed signal
        line = self.textCursor().blockNumber() + 1
        lines = self.blockCount()
        column = self.textCursor().columnNumber() + 1
        length = self.document().characterCount()
        self.cursorPositionChange.emit(line, column, length, lines)

    def wheelEvent(self, event):
        if event.modifiers() == Qt.ControlModifier:
            delta = event.angleDelta().y()
            if delta > 0:
                self.zoom_in()
            else:
                self.zoom_out()
        QPlainTextEdit.wheelEvent(self, event)

    def keyPressEvent(self, event):
        key = event.key()
        if key == Qt.Key_Tab:
            self._add_indentation()
            return
        QPlainTextEdit.keyPressEvent(self, event)
        if key in (Qt.Key_BracketLeft, Qt.Key_BraceLeft, Qt.Key_ParenLeft):
            # Complete braces
            current_brace = event.text()
            par_brace = Editor.MAP_BRACKETS.get(current_brace)
            self.textCursor().insertText(par_brace)
            self.moveCursor(QTextCursor.Left)
            return

    def _add_indentation(self):
        length = Settings.INDENTATION_WIDTH
        self.textCursor().insertText(" " * length)

    def getCursorPosition(self):
        """ Return the current cursor position at (line, column) """

        line = self.textCursor().blockNumber() + 1
        col = self.textCursor().columnNumber() + 1
        return line, col

    def indentationWidth(self):
        """ Returns the indentation width in characters """

        pass

    def set_cursor_position(self, position):
        tc = self.textCursor()
        tc.setPosition(position)
        self.setTextCursor(tc)

    def zoom_in(self):
        default_font = self.default_font()
        point_size = default_font.pointSize()
        point_size += 1
        default_font.setPointSize(point_size)
        self.setFont(default_font)
        self.__update_margin(default_font)

    def zoom_out(self):
        default_font = self.default_font()
        point_size = default_font.pointSize()
        # Minimum point size
        if point_size > 5:
            point_size -= 1
            default_font.setPointSize(point_size)
            self.setFont(default_font)
        self.__update_margin(default_font)

    def default_font(self):
        """ Return default font use by document """

        return self.document().defaultFont()

    def __update_margin(self, font):
        """ Update margin line depending of font point size """

        fm_width = QFontMetrics(self.default_font()).width('#')
        self.margin_position = fm_width * Settings.MARGIN_LINE_WIDTH