# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)


class Base(object):

    INDENTATION_WIDTH = 4
    USE_TABS = False

    def __init__(self, editor):
        self._editor = editor

    def indent(self):
        pass

    def unindent(self):
        pass



class PythonIndenter(Base):

    def __init__(self):
        super(PythonIndenter, self).__init__()
