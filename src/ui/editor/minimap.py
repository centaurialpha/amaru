# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QPlainTextEdit,
    QGraphicsOpacityEffect,
    QFrame
)
from PyQt5.QtGui import (
    QTextOption,
    QPainter,
    QPen,
    QFontMetrics
)
from PyQt5.QtCore import (
    Qt,
    QPropertyAnimation
)


class Minimap(QPlainTextEdit):

    def __init__(self, editor):
        super(Minimap, self).__init__(editor)
        self._editor = editor
        # Configuración
        self.setReadOnly(True)
        self.setCenterOnScroll(True)
        self.setMouseTracking(True)
        self.viewport().setCursor(Qt.PointingHandCursor)
        self.setTextInteractionFlags(Qt.NoTextInteraction)
        self.setWordWrapMode(QTextOption.NoWrap)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)

        self.setStyleSheet("background: transparent; border: 0px;")

        self.lines = 0
        self.slider = Slider(self)

    def __calculate_max(self):
        line_height = self._editor.cursorRect().height()
        if line_height > 0:
            self.lines = self._editor.viewport().height() / line_height
        self.slider.update_position()
        self.update_area()

    def update_area(self):
        line = self._editor.firstVisibleBlock().blockNumber()
        block = self.document().findBlockByLineNumber(line)
        cursor = self.textCursor()
        cursor.setPosition(block.position())
        rect = self.cursorRect(cursor)
        self.setTextCursor(cursor)

    def update_text(self):
        text = self._editor.toPlainText()
        self.setPlainText(text)
        self.__calculate_max()

    def resize(self):
        # Fijo la altura
        self.setFixedHeight(self._editor.height())
        # Fijo el ancho
        #FIXME: configurable
        self.setFixedWidth(self._editor.width() * 0.10)
        # Muevo el minimapa hacia la derecha del editor
        self.move(self._editor.width() - self.width(), 0)
        font = self.document().defaultFont()
        font_point_size = self.width() / 50
        font.setPixelSize(font_point_size)
        font.setBold(True)
        self.setFont(font)
        self.__calculate_max()

    def resizeEvent(self, event):
        QPlainTextEdit.resizeEvent(self, event)
        self.slider.update_position()


class Slider(QFrame):

    def __init__(self, minimap):
        super(Slider, self).__init__(minimap)
        self.show()
        self.setMouseTracking(True)
        self.setCursor(Qt.OpenHandCursor)
        # Animación
        self.go = QGraphicsOpacityEffect()
        self.setGraphicsEffect(self.go)
        self.go.setOpacity(0)
        self.animation = QPropertyAnimation(self.go, "opacity")
        self.animation.setDuration(200)
        self._minimap = minimap

    def paintEvent(self, event):
        painter = QPainter()
        painter.begin(self)
        painter.setRenderHint(QPainter.TextAntialiasing, True)
        painter.setRenderHint(QPainter.Antialiasing, True)
        painter.fillRect(self.rect(), Qt.lightGray)
        painter.setPen(QPen(Qt.NoPen))
        painter.end()
        super(Slider, self).paintEvent(event)

    def update_position(self):
        fm = QFontMetrics(self._minimap.font()).height()
        height = self._minimap.lines * fm
        self.setFixedHeight(height)
        self.setFixedWidth(self._minimap.width())

    def enterEvent(self, event):
        self.animation.setStartValue(0)
        self.animation.setEndValue(0.6)
        self.animation.start()

    def leaveEvent(self, event):
        self.animation.setStartValue(0.6)
        self.animation.setEndValue(0)
        self.animation.start()