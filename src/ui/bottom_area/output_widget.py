# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

import time

from PyQt5.QtWidgets import (
    QPlainTextEdit,
    QWidget,
    QVBoxLayout,

)
from PyQt5.QtGui import QTextCursor
from PyQt5.QtCore import (
    #QObject,
    QProcess
)

from src.core import settings


class Process(QWidget):

    def __init__(self):
        super(Process, self).__init__()
        box = QVBoxLayout(self)
        box.setContentsMargins(0, 0, 0, 0)
        self._process = QProcess(self)
        self._output = OutputWidget(self)
        box.addWidget(self._output)

        # Connections
        self._process.readyReadStandardOutput.connect(
            self._output.update_output)
        self._process.readyReadStandardError.connect(
            self._output.update_error)
        self._process.started.connect(
            self._output.process_started)
        self._process.finished.connect(
            self._output.process_finished)

    def run_code(self, program, filename):
        args = settings.PROGRAM_ARGS.get(program)
        self._process.start(program, args + [filename])

    def run_selection(self, program, text, args=['-c']):
        if program == 'ruby':
            args = ['-e']
        self._process.start(program, args + [text])


class OutputWidget(QPlainTextEdit):

    def __init__(self, parent):
        super(OutputWidget, self).__init__(parent)
        style = "QPlainTextEdit {color: #1d1d1c; border: none;}"
        self.setStyleSheet(style)
        self.setReadOnly(True)
        self.setUndoRedoEnabled(False)
        self.__set_font()
        self._parent = parent
        self._process = parent._process

    def __set_font(self):
        font = self.document().defaultFont()
        font.setPointSize(16)
        self.setFont(font)

    def update_output(self):
        data = self._process.readAllStandardOutput().data().decode('utf8')
        self.textCursor().insertText(data)

    def update_error(self):
        data = self._process.readAllStandardError().data().decode('utf8')
        for text in data.splitlines():
            self.textCursor().insertText(text)
            self.textCursor().insertBlock()

    def process_started(self):
        self.clear()
        self._process_time = time.time()

    def process_finished(self, exit_code, exit_status):
        self._process_time = time.time() - self._process_time
        self.textCursor().insertText(self.tr("[Finished in {0:.2f}s]".format(
            self._process_time)))
        self.moveCursor(QTextCursor.EndOfLine)
        self.textCursor().insertBlock()