# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QWidget,
    QStackedWidget,
    QToolBar,
    QSizePolicy,
    #QTabBar,
    #QTabWidget,
    #QStyle,
    #QStyleOptionTab,
    QVBoxLayout,
    #QStylePainter
)
from PyQt5.QtGui import QIcon
#from PyQt5.QtCore import (
    #Qt,
    #QSize
#)

from src import ui
from src.ui.bottom_area import output_widget
from src.core import (
    file_manager,
    settings
)


class _BottomContainer(QWidget):

    def __init__(self, parent=None):
        super(_BottomContainer, self).__init__(parent)

        box = QVBoxLayout(self)
        box.setContentsMargins(0, 0, 0, 0)

        # Toolbar
        self._toolbar = ToolBar(self)
        box.addWidget(self._toolbar)

        # Stacked
        self._stack = QStackedWidget()
        box.addWidget(self._stack)

        self._output = output_widget.Process()
        self.add_widget(self._output, ":img/terminal", self.tr("Output"))

        ui.install_service("bottom_container", self)

    def add_widget(self, widget, icon, title):
        self._stack.addWidget(widget)
        self._toolbar.addAction(title, icon)

    def execute_file(self, filename):
        extension = file_manager.get_extension(filename)
        program = settings.SUPPORTED_FILES.get(extension, None)
        if program is not None:
            self._output.run_code(program, filename)

    def execute_selection(self, text, filename):
        extension = file_manager.get_extension(filename)
        program = settings.SUPPORTED_FILES.get(extension, None)
        if program is not None:
            self._output.run_selection(program, text)


class ToolBar(QToolBar):
    """ Custom Toolbar with actions align on center """

    def __init__(self, parent=None):
        super(ToolBar, self).__init__(parent)
        # Left separator
        left_separator = QWidget()
        left_separator.setSizePolicy(QSizePolicy.Expanding,
                                     QSizePolicy.Expanding)
        self.addWidget(left_separator)
        self.right_separator = False

    def addAction(self, text, icon=None):
        if icon is not None:
            super(ToolBar, self).addAction(QIcon(icon), text)
        else:
            super(ToolBar, self).addAction(text)
        right_separator = QWidget()
        right_separator.setSizePolicy(QSizePolicy.Expanding,
                                      QSizePolicy.Expanding)
        if self.right_separator:
            widget = self.widgetForAction(self.qaction)
            widget.deleteLater()
            self.right_separator = False

        self.qaction = self.addWidget(right_separator)
        self.right_separator = True

bottom_container = _BottomContainer()