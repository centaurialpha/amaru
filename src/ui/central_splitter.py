# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QSplitter
)
from PyQt5.QtCore import (
    Qt,
    QSettings
)

from src import ui
from src import SETTINGS_FILE


class CentralSplitter(QSplitter):
    """
    This widget divided into three areas UI.
    1. Lateral area
    2. Bottom area
    3. Tabs
    """

    def __init__(self, orientation=Qt.Vertical, parent=None):
        super(CentralSplitter, self).__init__(orientation, parent)
        self._horizontal_splitter = QSplitter(Qt.Horizontal)
        self.addWidget(self._horizontal_splitter)

        # Install service
        ui.install_service("central_splitter", self)

    def load_ui(self):
        """ Load lateral, bottom area and main panel """

        self._lateral_container = ui.get_instance("lateral_container")
        self._horizontal_splitter.addWidget(self._lateral_container)
        self._main_panel = ui.get_instance("main_panel")
        self._horizontal_splitter.addWidget(self._main_panel)
        self._bottom_container = ui.get_instance("bottom_container")
        self.addWidget(self._bottom_container)

    def showEvent(self, event):
        QSplitter.showEvent(self, event)
        qsettings = QSettings(SETTINGS_FILE, QSettings.IniFormat)
        right = self._horizontal_splitter.width() / 2
        hsizes = qsettings.value('central_splitter_hsizes', None)

        if hsizes is not None:
            self._horizontal_splitter.restoreState(hsizes)
        else:
            self._horizontal_splitter.setSizes([1, right])
        vsizes = qsettings.value('central_splitter_vsizes', None)
        if vsizes is not None:
            self.restoreState(vsizes)
        else:
            self.setSizes([(self.height() / 3) * 2, self.height() / 3])

        if not qsettings.value('central_splitter_bottom_visible',
                                True, type=bool):
            self._bottom_container.hide()
        if not qsettings.value('central_splitter_lateral_visible',
                                True, type=bool):
            self._lateral_container.hide()

    def save_sizes(self):
        qsettings = QSettings(SETTINGS_FILE, QSettings.IniFormat)
        qsettings.setValue('central_splitter_hsizes',
                           self._horizontal_splitter.saveState())
        qsettings.setValue('central_splitter_vsizes', self.saveState())
        qsettings.setValue('central_splitter_lateral_visible',
                           self._lateral_container.isVisible())
        qsettings.setValue('central_splitter_bottom_visible',
                           self._bottom_container.isVisible())

    def lateral_is_visible(self):
        return self._lateral_container.isVisible()

    def bottom_is_visible(self):
        return self._bottom_container.isVisible()

    def bottom_visibility(self, value):
        if not value:
            self._bottom_container.hide()
        else:
            self._bottom_container.show()

    def lateral_visibility(self, value):
        if not value:
            self._lateral_container.hide()
        else:
            self._lateral_container.show()


central_splitter = CentralSplitter()