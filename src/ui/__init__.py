# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)


SERVICES = {}


def install_service(name, instance):
    SERVICES[name] = instance


def get_instance(name):
    return SERVICES.get(name, None)