# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QMenu,
    QActionGroup,
    QAction,
    QStyleFactory,
    QApplication
)

from src import ui
from src.core import keymap
from src.core.settings import Settings


class Menu(QMenu):
    """
    Clase base para todos los menús. Cada menú está representado
    por una clase y deberá ser subclase de ésta clase, implementado el
    método `build()`.
    """

    # Atajos
    keyMap = keymap.KEYMAP
    toolbar = {}

    def __init__(self, parent=None, name=None):
        super(Menu, self).__init__(parent)
        if name is not None:
            self.setTitle(name)

        #self.toolbar = {}
        # Construyo todos los menús
        self.build()
        #self.build_toolbar()

    def build(self):
        NotImplementedError("Implementar éste método")

    def build_toolbar(self):
        for attr in dir(self):
            if attr.endswith('action') and not attr.startswith('_'):
                qaction = getattr(self, attr)
                self.toolbar[attr] = qaction

    def __add_action(self, text, checkable=None):
        """ Esta función crea un QAction """

        action = self.addAction(text)

        if checkable is not None:
            action.setCheckable(True)
            action.setChecked(checkable)

        return action

    def add_item(self, text, func=None):
        qaction = self.__add_action(text)

        if func is not None:
            qaction.triggered.connect(func)

        return qaction

    def add_check_item(self, text, func=None, checked=False):
        qaction = self.__add_action(text, checked)
        if func is not None:
            qaction.triggered.connect(
                lambda b=None: func(qaction.isChecked()))

        return qaction


class FileMenu(Menu):

    def build(self):
        main_panel = ui.get_instance("main_panel")
        main = ui.get_instance("main")

        # Nuevo archivo
        func = main_panel.add_new_editor
        self.new_action = self.add_item(self.tr("New"), func)
        self.new_action.setShortcut(self.keyMap['new-file'])

        # Abrir archivo
        func = main_panel.open_file
        self.open_action = self.add_item(self.tr("Open..."), func)
        self.open_action.setShortcut(self.keyMap['open-file'])

        # Open folder
        func = main_panel.open_folder
        self.open_folder_action = self.add_item(self.tr("Open Folder"),
                                                func=func)

        # Guardar archivo
        func = main_panel.save_file
        self.save_action = self.add_item(self.tr("Save"), func)
        self.save_action.setShortcut(self.keyMap['save'])

        # Guardar como
        func = main_panel.save_file_as
        self.save_as_action = self.add_item(self.tr("Save As..."), func)
        self.save_as_action.setShortcut(self.keyMap['save-as'])
        self.addSeparator()

        # Close
        func = main_panel.close_tab
        self.close_tab_action = self.add_item(self.tr("Close"), func)
        self.close_tab_action.setShortcut(self.keyMap['close'])

        # Close all
        func = main_panel.close_all_tabs
        self.close_all_tabs_action = self.add_item(self.tr("Close All"), func)

        # Close all except this
        func = main_panel.close_all_tabs_except_this
        self.close_all_except_action = self.add_item(
            self.tr("Close All Except This"), func)
        self.addSeparator()

        # Restart Amaru
        func = main.restart
        self.restart_action = self.add_item(self.tr("Restart"), func=func)
        # Salir
        self.exit_action = self.add_item(self.tr("Exit"), main.close)
        self.exit_action.setShortcut(self.keyMap['exit'])


class EditMenu(Menu):

    def build(self):
        self.undo_action = self.add_item(self.tr("Undo"))
        self.redo_action = self.add_item(self.tr("Redo"))
        self.addSeparator()
        self.cut_action = self.add_item(self.tr("Cut"))
        self.copy_action = self.add_item(self.tr("Copy"))
        self.paste_action = self.add_item(self.tr("Paste"))


class ViewMenu(Menu):

    def build(self):
        main_panel = ui.get_instance("main_panel")
        main = ui.get_instance("main")
        central = ui.get_instance("central_splitter")

        # Show/hide bottom area
        self.show_bottom_area_action = self.add_check_item(
            self.tr("Show Bottom Area"),
            func=lambda v: central.bottom_visibility(v),
            checked=main.settings().value('central_splitter_bottom_visible',
                                          type=bool))

        # Show/hide lateral area
        self.show_lateral_area_action = self.add_check_item(
            self.tr("Show Lateral Area"),
            func=lambda v: central.lateral_visibility(v),
            checked=main.settings().value('central_splitter_lateral_visible',
                                          type=bool))
        self.addSeparator()

        # Split tabs vertically
        func = lambda v: main_panel.split_editor(1)
        self.splitv_action = self.add_item(self.tr("Split Editor Vertically"),
                                           func)
        self.splitv_action.setShortcut(self.keyMap['split-v'])

        # Split tabs horizontally
        func = lambda v: main_panel.split_editor(2)
        self.splith_action = self.add_item(self.tr("Split Edito Horizontally"),
                                           func)
        self.splith_action.setShortcut(self.keyMap['split-h'])
        self.addSeparator()

        func = main_panel.show_line_numbers
        self.line_numbers_visibility_action = self.add_item(
            self.tr("Show Line Numbers"), func)
        self.addSeparator()

        # Zoom +
        func = main_panel.editor_zoom_in
        self.zoom_in_action = self.add_item(self.tr("Zoom In"), func)
        self.zoom_in_action.setShortcut(self.keyMap['zoom-in'])

        # Zoom -
        func = main_panel.editor_zoom_out
        self.zoom_out_action = self.add_item(self.tr("Zoom Out"), func)
        self.zoom_out_action.setShortcut(self.keyMap['zoom-out'])
        self.addSeparator()

        # Styles
        s = StylesSubMenu(self, self.tr("Style"))
        self.addMenu(s)


class StylesSubMenu(Menu):

    def build(self):
        qactions = {}

        styles = QStyleFactory.keys()
        styles = [style.lower() for style in styles]

        if not Settings.STYLE:
            Settings.STYLE = QApplication.style().objectName()

        for style in styles:
            qaction = QAction(style, self, checkable=True)
            if style == Settings.STYLE.lower():
                qaction.setChecked(True)
            self.addAction(qaction)
            qactions[style] = qaction

        group = QActionGroup(self)
        for name, ac in qactions.items():
            ac.name = name
            ac.triggered.connect(self.__change_qt_style)
            group.addAction(ac)

    def __change_qt_style(self):
        style = self.sender().name
        QApplication.setStyle(style)


class RunMenu(Menu):

    def build(self):
        main_panel = ui.get_instance("main_panel")

        func = main_panel.execute_file
        self.execute_file_action = self.add_item(self.tr("Execute File"), func)

        func = main_panel.execute_selection
        self.execute_selection = self.add_item(self.tr("Execute Selection"),
                                               func)


def build(menubar, menus):
    """
    Esta función crea cada uno de los objeto menú y los agrega a la
    barra de menu (QMenuBar)
    """

    for name, menu in menus.items():
        menu_instance = menu(menubar, name)
        menubar.addMenu(menu_instance)