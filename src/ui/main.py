# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

# Módulos Python
#import os
#import webbrowser
from collections import OrderedDict

# Módulos QtGui
from PyQt5.QtWidgets import (
    QMainWindow,
    QShortcut,
    QApplication,
    #QToolBar,
    #QMessageBox
)
from PyQt5.QtGui import (
    QIcon,
    QKeySequence,
)
from PyQt5.QtCore import (
    QProcess,
    Qt,
    QSettings
)
#from PyQt5.QtGui import QIcon

#from src.core import settings
#from src.core.settings import Settings
from src import (
    ui,
    SETTINGS_FILE
)
from src.ui import menu


class Amaru(QMainWindow):

    def __init__(self):
        QMainWindow.__init__(self)
        self.setWindowTitle('{Amaru}')
        # Tamaño de la ventana por defecto
        self.setMinimumSize(750, 500)
        # Load size and position of main window
        self._load_window_geometry()
        ui.install_service("main", self)

        # Widget central
        central_splitter = ui.get_instance("central_splitter")
        central_splitter.load_ui()
        self.setCentralWidget(central_splitter)

        # Create menus
        menus = OrderedDict([
            (self.tr("&File"), menu.FileMenu),
            (self.tr("&Edit"), menu.EditMenu),
            (self.tr("&View"), menu.ViewMenu),
            (self.tr("&Run"), menu.RunMenu)
        ])
        menu.build(self.menuBar(), menus)

        #self.toolbar = QToolBar(self)
        ##FIXME: testssssssss
        #self.toolbar.addAction(QIcon(":img/edit"), '')
        #self.toolbar.addAction(QIcon(":img/gear"), '')
        #self.addToolBar(1, self.toolbar)
        # Status bar
        #status_bar = ui.get_instance("status_bar")
        #self.setStatusBar(status_bar)

        # Create shortcut to change tab index with ALT + [1-9]
        key = -1
        for i in range(48, 58):
            shortcut = QShortcut(QKeySequence(Qt.ALT + i), self)
            shortcut.index = key
            shortcut.activated.connect(self._change_tab_index)
            key += 1

        #main_panel = ui.get_instance("main_panel")
        # Conecctions
        #main_panel.cursorPositionChange[int, int, int, int].connect(
            #self._update_cursor_position)
        #main_panel.fileSaved.connect(status_bar.show_text)
        #main_panel.currentTabChanged.connect(status_bar.update_filename_label)
        #main_panel.allTabsClosed.connect(status_bar.hide_all)

    def _change_tab_index(self):
        obj = self.sender()
        main_panel = ui.get_instance("main_panel")
        main_panel.change_tab_index(obj.index)

    def _load_window_geometry(self):
        qsettings = QSettings(SETTINGS_FILE, QSettings.IniFormat)
        show_maximized = qsettings.value('window_maximized', True, type=bool)
        if show_maximized or show_maximized is None:
            self.showMaximized()
        else:
            size = qsettings.value('window_size')
            self.resize(size)
            position = qsettings.value('window_position')
            self.move(position)

    def show_hide_toolbar(self):
        """ Toggles the visibility of the status bar """

        state = self.toolbar.isVisible()
        self.toolbar.setVisible(not state)

    def _update_cursor_position(self, line, col, length, lines):
        status_bar = ui.get_instance("status_bar")
        status_bar.update_cursor_position(line, col, length, lines)

    #def __load_ui(self, central_splitter):
        #lateral_container = ui.get_instance("lateral_container")
        ##central_splitter.install_area('lateral_container',
                                      ##lateral_container, True)

        #main_panel = ui.get_instance("main_panel")
        ##central_splitter.install_area('main_panel', main_panel, True)

        #bottom_container = ui.get_instance("bottom_container")
        #central_splitter.install_area('bottom_container', bottom_container)

    def settings(self):
        return QSettings(SETTINGS_FILE, QSettings.IniFormat)

    def closeEvent(self, event):
        # Save settings
        qsettings = QSettings(SETTINGS_FILE, QSettings.IniFormat)
        if not self.isMaximized():
            qsettings.setValue('window_maximized', False)
            qsettings.setValue('window_position', self.pos())
            qsettings.setValue('window_size', self.size())
        else:
            qsettings.setValue('window_maximized', True)

        central = ui.get_instance("central_splitter")
        central.save_sizes()

    def restart(self):
        """ Restart Amaru """

        args = QApplication.arguments()
        QProcess.startDetached(QApplication.applicationFilePath(), args)
        QApplication.quit()
