# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from collections import OrderedDict

from PyQt5.QtWidgets import (
    QWidget,
    QComboBox,
    QStackedWidget,
    QVBoxLayout
)

from src import ui
from src.ui.lateral_area import (
    explorer,
    projects_widget
)


class _LateralContainer(QWidget):

    MEMBERS = OrderedDict()

    def __init__(self, parent=None):
        super(_LateralContainer, self).__init__(parent)

        box = QVBoxLayout(self)
        box.setContentsMargins(0, 0, 0, 0)
        self._combo = QComboBox()
        box.addWidget(self._combo)

        # Stacked
        self.stack = QStackedWidget()
        box.addWidget(self.stack)

        # Explorer
        #self._explorer = explorer.Explorer()
        #self.add_widget(self._explorer, self.tr("Explorer"))

        # Projects widget
        #self._projects_tree = projects_widget.ProjectsTree()
        #self.add_widget(self._projects_tree, self.tr("Folders"))

        # Connections
        self._combo.currentIndexChanged[int].connect(
            lambda index: self.stack.setCurrentIndex(index))

        # Install service
        ui.install_service("lateral_container", self)

    def add_widget(self, widget, name):
        self.stack.addWidget(widget)
        self._combo.addItem(name)

    def open_folder(self, structure, root):
        self._combo.setCurrentIndex(1)
        self.stack.setCurrentIndex(1)
        self._projects_tree.load_folder(structure, root)

lateral_container = _LateralContainer()