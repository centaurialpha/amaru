# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QTreeView,
    QFileSystemModel
)
from PyQt5.QtGui import QIcon
from PyQt5.QtCore import (
    QModelIndex,
    QDir,
    Qt
)

from src import ui
from src.core.settings import SUPPORTED_FILES


class Explorer(QTreeView):

    def __init__(self, parent=None):
        super(Explorer, self).__init__(parent)
        self.header().setHidden(True)
        self.setAnimated(True)

        # Model
        self.model = FileSystemModel(self)
        home_path = QDir.toNativeSeparators(QDir.homePath())
        self.model.setRootPath(home_path)
        self.setModel(self.model)
        self.model.setNameFilters(['*.' + ext for ext in SUPPORTED_FILES])
        self.setRootIndex(QModelIndex(self.model.index(home_path)))
        self.model.setNameFilterDisables(False)

        # Hide some columns
        for i in range(1, 4):
            self.hideColumn(i)

        self.doubleClicked.connect(self.__open_file)

    def __open_file(self, index):
        if not self.model.isDir(index):
            filename = self.model.filePath(index)
            main_panel = ui.get_instance('main_panel')
            main_panel.open_file(filename)


class FileSystemModel(QFileSystemModel):

    def __init__(self, parent=None):
        super(FileSystemModel, self).__init__(parent)

        self._file_types = {
            'file': QIcon(":img/file-text"),
            'folder': QIcon(":img/folder"),
        }

    def data(self, index, role):
        if role == Qt.DecorationRole:
            info = self.fileInfo(index)
            if info.isFile():
                return self._file_types.get('file')
            else:
                return self._file_types.get('folder')
        return QFileSystemModel.data(self, index, role)
