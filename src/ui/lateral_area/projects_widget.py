# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

import os

from PyQt5.QtWidgets import (
    QTreeWidget,
    QTreeWidgetItem
)
from PyQt5.QtGui import QIcon

from src.core import file_manager
from src import ui


class ProjectsTree(QTreeWidget):

    def __init__(self, parent=None):
        super(ProjectsTree, self).__init__(parent)
        self.setAnimated(True)
        self.header().setHidden(True)

        self.itemClicked.connect(self.__open_file)

    def __open_file(self, item, column):
        if item.is_file:
            main_panel = ui.get_instance("main_panel")
            main_panel.open_file(item.path)

    def load_folder(self, structure, root):
        root_name = file_manager.get_folder_name(root)
        parent = QTreeWidgetItem(self, [root_name])
        parent.setExpanded(True)
        # Folder name
        self.__load_tree(structure, parent, root)

    def __load_tree(self, structure, parent, root):
        files, folders = structure.get(root)
        for fo in sorted(folders):
            folder_item = Item(parent, [fo])
            folder_item.is_file = False
            folder_item.path = os.path.join(root, fo)
            folder_item.setIcon(0, QIcon(":img/folder"))
            folder_item.setToolTip(0, folder_item.path)
            self.__load_tree(structure, folder_item, folder_item.path)
        for f in sorted(files):
            file_item = Item(parent, [f])
            file_item.path = os.path.join(root, f)
            file_item.setIcon(0, QIcon(":img/file-text"))
            file_item.setToolTip(0, file_item.path)


class Item(QTreeWidgetItem):
    """ Custom Tree Widget Item """

    def __init__(self, parent=None, name=''):
        super(Item, self).__init__(parent, name)
        self.is_file = True
        self.path = ''