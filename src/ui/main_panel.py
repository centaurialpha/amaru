# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

import os

from PyQt5.QtWidgets import (
    QSplitter,
    QFileDialog,
    QMessageBox
)
from PyQt5.QtCore import pyqtSignal

from src import ui
from src.ui.editor import editor
from src.ui import tab_widget
from src.core import (
    file_manager,
    object_file
)
from src.core.settings import Settings


class MainPanel(QSplitter):
    """
    MainPanel class

    This class represents the central widget, the actions are implemented
    in this class.
    Contains the editors, start page and preferences widget.
    """

    # Signals
    cursorPositionChange = pyqtSignal(int, int, int, int)
    fileSaved = pyqtSignal('QString')
    currentTabChanged = pyqtSignal('QString')
    allTabsClosed = pyqtSignal()

    def __init__(self, parent=None):
        super(MainPanel, self).__init__(parent)
        # Primary and secundary tab
        self._primary_tab = tab_widget.TabWidget(self)
        self.addWidget(self._primary_tab)
        self._secundary_tab = tab_widget.TabWidget(self)
        self._secundary_tab.hide()
        self.addWidget(self._secundary_tab)
        self.setSizes([1, 1])

        # Actual tab
        self._tab = self._primary_tab

        # To remember the last folder acceded
        self.__last_open_folder = None

        # Connections
        self._tab.currentChanged[int].connect(self.__current_tab_changed)
        # Install the service
        ui.install_service("main_panel", self)

    def __current_tab_changed(self, index):
        if index == -1:
            self.allTabsClosed.emit()
        else:
            widget = self._tab.widget(index)
            self.currentTabChanged.emit(widget.fobject.filename)

    def __document_has_been_modified(self, value):
        self._tab.tab_modified(value)

    def add_new_editor(self, filename=''):
        """
        Ésta función crea un objeto Editor y la agrega a las pestañas, el
        parámetro `filename` es el nombre completo del archivo.
        """

        file_ = object_file.File(filename)
        file_.fileChange.connect(self.__on_file_changed)

        weditor = editor.create_new_editor(file_)
        inserted_index = self.add_new_tab(weditor, file_.display_name)
        # Agrego el tooltip
        self._tab.setTabToolTip(inserted_index, file_.filename)

        # Conexiones
        weditor.modificationChanged[bool].connect(
            self.__document_has_been_modified)
        weditor.focusChanged.connect(self._on_focus_changed)
        weditor.cursorPositionChange[int, int, int, int].connect(
            lambda ln, col, len_, lns: self.cursorPositionChange.emit(
                ln, col, len_, lns))

        return weditor

    def open_file(self, filename='', cursor_position=0):
        if not filename:
            if self.__last_open_folder is None:
                dir_ = os.path.expanduser("~")
            else:
                dir_ = self.__last_open_folder
            filenames = QFileDialog.getOpenFileNames(self,
                                                     self.tr("Open File"),
                                                     dir_)[0]
            if not filenames:
                return

            # Save last folder
            self.__last_open_folder = file_manager.get_path_name(filenames[0])
        else:
            filenames = [filename]

        for filename in filenames:
            weditor = self.add_new_editor(filename)
            # Read file content
            content = weditor.fobject.read()
            # Add the content to the editor
            weditor.text = content
            # Start system watcher
            weditor.fobject.start_watching()

            # Set cursor position
            if cursor_position != 0:
                weditor.set_cursor_position(cursor_position)
            weditor.modified = False

    def open_folder(self, directory=''):
        if not directory:
            directory = QFileDialog.getExistingDirectory(self,
                                                         self.tr("Open Folder"))
            if not directory:
                return

        folder_structure = {}
        exclude = set(['.git', '__pycache__'])
        for root, directories, files in os.walk(directory, topdown=True):
            directories[:] = [d for d in directories if d not in exclude]
            files = [f for f in files]
            folder_structure[root] = (files, directories)

        lateral_container = ui.get_instance("lateral_container")
        lateral_container.open_folder(folder_structure, directory)

    def save_file(self, weditor=None):
        if not weditor:
            weditor = self.get_active_editor()
            if weditor is None:
                return
        #if weditor is not None:
        fobject = weditor.fobject
        if fobject.is_new:
            return self.save_file_as(weditor)

        # Get the from editor
        content = weditor.text
        # Update file with the new content
        fobject.write(content)
        weditor.modified = False

        self.fileSaved.emit(
            self.tr("File saved: {}".format(fobject.filename)))
        #return True

    def save_file_as(self, weditor):
        filename = QFileDialog.getSaveFileName(self, self.tr("Save File"))[0]

        if not filename:
            return False

        fobject = weditor.fobject
        content = weditor.text
        fobject.write(content, filename)
        weditor.modified = False

        self.fileSaved.emit(
                self.tr("File saved: {}".format(fobject.filename)))
        return True

    def close_tab(self):
        index = self._tab.currentIndex()
        self._tab.remove_tab(index)

    def close_all_tabs(self):
        self._tab.remove_all_tabs()

    def close_all_tabs_except_this(self):
        self._tab.remove_all_tabs_except_this()

    def add_new_tab(self, widget, title):
        return self._tab.add_widget(widget, title)

    def split_editor(self, orientation):
        if self._tab.count() <= 1:
            if not self._secundary_tab.isVisible():
                return
            self.__split_editor(orientation, True)
        else:
            self.__split_editor(orientation, False)

    def __split_editor(self, orientation, splitted=False):
        current_widget = self._tab.currentWidget()
        text = self._tab.tabText(self._tab.currentIndex())
        if not splitted and not self._secundary_tab.count() > 1:
            self._secundary_tab.add_widget(current_widget, text)
            self._secundary_tab.show()
            self._tab = self._secundary_tab
        else:
            for index in range(self._secundary_tab.count()):
                widget = self._secundary_tab.widget(0)
                tab_text = self._secundary_tab.tabText(0)
                self._primary_tab.add_widget(widget, tab_text)
            self._secundary_tab.hide()
            self._tab = self._primary_tab
        index = self._tab.indexOf(current_widget)
        self._tab.setCurrentIndex(index)
        self.setOrientation(orientation)
        self.setSizes([1, 1])

    def _on_focus_changed(self, widget):
        #FIXME: focusInEvent no funciona en QTabWidget
        tab_widget = widget.parentWidget().parentWidget()
        self._tab = tab_widget

    def add_start_page(self):
        pass

    def show_preferences(self):
        pass

    def editor_zoom_in(self):
        weditor = self.get_active_editor()
        if weditor is not None:
            weditor.zoom_in()

    def editor_zoom_out(self):
        weditor = self.get_active_editor()
        if weditor is not None:
            weditor.zoom_out()

    def get_active_editor(self):
        """
        This function returns an Editor object if widget in the actual tab
        is an Editor object, None otherwise.
        """

        widget = self._tab.currentWidget()
        if isinstance(widget, editor.Editor):
            return widget
        return None

    def get_opened_files(self):
        files = []

        for index in range(self._primary_tab.count()):
            widget = self._primary_tab.widget(index)
            if isinstance(widget, editor.Editor):
                filename = widget.fobject.filename
                cursor_pos = widget.get_cursor_position()
                files.append((filename, cursor_pos))

        for index in range(self._secundary_tab.count()):
            widget = self._secundary_tab.widget(index)
            if isinstance(widget, editor.Editor):
                filename = widget.fobject.filename
                cursor_pos = widget.get_cursor_position()
                files.append((filename, cursor_pos))

        return files

    def change_tab_index(self, index=-1):
        if index != -1:
            self._tab.setCurrentIndex(index)

    def execute_file(self):
        weditor = self.get_active_editor()
        if weditor is not None:
            self.save_file(weditor)
            bottom_container = ui.get_instance("bottom_container")
            filename = weditor.fobject.filename
            bottom_container.execute_file(filename)

    def execute_selection(self):
        weditor = self.get_active_editor()
        if weditor is not None:
            text = weditor.textCursor().selectedText()
            if text:
                bottom_container = ui.get_instance("bottom_container")
                fname = weditor.fobject.filename
                bottom_container.execute_selection(text, fname)

    def show_line_numbers(self):
        weditor = self.get_active_editor()
        if weditor is not None:
            weditor.show_line_numbers()
        #Settings.SHOW_LINE_NUMBERS = not Settings.SHOW_LINE_NUMBERS

    def __on_file_changed(self):
        QMessageBox.information(self, self.tr("File Changed"),
                                self.tr("The file '{}' has changed outside "
                                        "Amaru. Do you want "
                                        "to reload it?"))

mainPanel = MainPanel()
