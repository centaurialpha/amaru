# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QStatusBar,
    QLabel
)

from src import ui


class _StatusBar(QStatusBar):

    def __init__(self, parent=None):
        super(_StatusBar, self).__init__(parent)
        style = "QStatusBar {border: none} QStatusBar::item {border: none}"
        self.setStyleSheet(style)

        self._filename_label = QLabel()
        self.addWidget(self._filename_label)

        self._cursor_info = "  {0}:{1}"
        self._cur_position_label = QLabel(self._cursor_info.format(1, 1,))
        self.hide_all()
        self.addWidget(self._cur_position_label)

        # Install service
        ui.install_service("status_bar", self)

    def update_cursor_position(self, line, column, length, lines):
        """ Update text in labels with cursor position information """

        text = self._cursor_info.format(line, column)
        self._cur_position_label.setText(text)

    def update_filename_label(self, text):
        self._filename_label.setText(text)

    def show_text(self, text, timeout=3000):
        """
        Displays the given text for the specified number of milli-seconds
        (timeout)
        """

        QStatusBar.showMessage(self, text, timeout)

    def hide_all(self):
        """ Hide all widgets """

        self._cur_position_label.setText("")
        self._filename_label.setText("")


status_bar = _StatusBar()
