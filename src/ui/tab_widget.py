# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtWidgets import (
    QTabWidget,
    QTabBar,
    QMenu,
    QMessageBox,
    QToolButton
)
from PyQt5.QtGui import QFontMetrics
from PyQt5.QtCore import (
    pyqtSignal,
    Qt,
    QSize
)

from src import ui
#from src.core.settings import Settings


class TabWidget(QTabWidget):

    focusChanged = pyqtSignal('PyQt_PyObject')

    def __init__(self, parent=None):
        QTabWidget.__init__(self, parent)
        # Set tab bar
        tab_bar = TabBar()
        tab_bar.plusButtonClicked.connect(self._add_new_widget)
        self.setTabBar(tab_bar)
        # Configuration
        self.setTabsClosable(True)
        self.setMovable(True)
        self.setAcceptDrops(True)
        self.setElideMode(1)

        #self.installEventFilter(self)

        # Menu
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self._context_menu)

        # Conecctions
        self.tabCloseRequested[int].connect(self.remove_tab)
        self.currentChanged[int].connect(self._on_tab_changed)
        #self.tabBar().barClicked.connect(self._add_new_widget)

    def _context_menu(self, point):
        """ Context Menu """

        index = self.currentIndex()
        point = self.tabBar().tabRect(index).bottomLeft()

        menu = QMenu()
        menu.addAction("lalala")

        menu.popup(self.tabBar().mapToGlobal(point))

    def _add_new_widget(self):
        main_panel = ui.get_instance("main_panel")
        main_panel.add_new_editor()

    def add_widget(self, widget, name):
        """ Agrega un widget en una pestaña y retorna el índice """

        index = self.addTab(widget, name)
        self.setCurrentIndex(index)
        widget.setFocus()
        return index

    def tab_modified(self, value):
        """
        Ésta función cambia el texto en la pestaña cuando el documento es
        modificado.
        """

        weditor = self.sender()
        fobject = weditor.fobject
        if value:
            text = "\u2022 {name}".format(name=fobject.display_name)
        else:
            text = fobject.display_name
        self.setTabText(self.currentIndex(), text)

    def remove_all_tabs(self):
        """ Remove all tabs """

        for index in range(self.count()):
            self.remove_tab(0)

    def remove_all_tabs_except_this(self):
        current_tab = self.widget(self.currentIndex())
        self.insertTab(0, current_tab, self.tabText(self.currentIndex()))
        self.setCurrentIndex(0)
        for index in reversed(range(self.count())):
            if index > 0:
                self.remove_tab(index)

    def remove_tab(self, index):
        widget = self.currentWidget()
        main_panel = ui.get_instance("main_panel")

        if widget.modified:
            flags = QMessageBox.Yes
            flags |= QMessageBox.Cancel
            flags |= QMessageBox.No
            name = widget.fobject.display_name
            r = QMessageBox.question(self, "",
                                     self.tr("<b>{}</b> has changes, do you "
                                             "want to save them?\n Your "
                                             "changes will be lost if close "
                                             "this item "
                                             "without saving.".format(name)),
                                     flags)
            if r == QMessageBox.Cancel:
                return
            if r == QMessageBox.Yes:
                if not main_panel.save_file():
                    return
        QTabWidget.removeTab(self, index)

    def _on_tab_changed(self, index):
        self.setCurrentIndex(index)
        widget = self.widget(index)
        if widget is not None:
            widget.setFocus()


class TabBar(QTabBar):

    plusButtonClicked = pyqtSignal()

    def __init__(self, parent=None):
        super(TabBar, self).__init__(parent)

        # plust button
        self.plus_button = QToolButton()
        self.plus_button.setText('+')
        self.plus_button.setAutoRaise(True)
        self.plus_button.setParent(self)
        self.plus_button.setMaximumSize(25, 25)
        self.plus_button.clicked.connect(
            lambda: self.plusButtonClicked.emit())

    def sizeHint(self):
        sizeh = QTabBar.sizeHint(self)
        return QSize(sizeh.width() + 25, sizeh.height())

    def resizeEvent(self, event):
        super(TabBar, self).resizeEvent(event)
        self.move_plus_button()

    def tabLayoutChange(self):
        super(TabBar, self).tabLayoutChange()
        self.move_plus_button()

    def move_plus_button(self):
        size = 0
        for i in range(self.count()):
            size += self.tabRect(i).width()

        height = self.geometry().top() + 10
        width = self.width()
        if size > width:
            self.plus_button.move(width - 54, height)
        else:
            self.plus_button.move(size, height)