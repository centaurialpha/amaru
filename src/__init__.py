# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

from PyQt5.QtCore import QDir

from src import resources  # lint:ok

# PATHS
HOME = QDir.home()

PROJECT_DIR = QDir.current().filePath('src')

HOME_PROJECT_DIR = HOME.filePath('.amaru')

SETTINGS_FILE = QDir(HOME_PROJECT_DIR).filePath('settings.ini')

LOG_FILE = QDir(HOME_PROJECT_DIR).filePath('logfile.log')

SYNTAX_FILES = QDir(PROJECT_DIR).filePath('syntax')
