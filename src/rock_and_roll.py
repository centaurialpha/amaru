# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

import sys

from src.core import (
    settings,
)

settings.read()

from src.ui.main import Amaru


def run_amaru(app):
    #lint:disable
    from src.ui import (
        central_splitter,
        #editor_panel,
        main_panel,
        status_bar
    )
    #lint:enable
    from src.ui.lateral_area import lateral_container  # lint:ok
    from src.ui.bottom_area import bottom_container  # lint:ok

   # with open("/home/gabo/style.qss") as f:
       # app.setStyleSheet(f.read())

    amaru = Amaru()
    amaru.show()

    sys.exit(app.exec_())
