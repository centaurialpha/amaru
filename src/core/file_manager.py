# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

import os

from PyQt5.QtCore import (
    QFileInfo
)

"""
Éste módulo contiene funciones útiles para el manejo de archivos
"""


def get_path_name(filename):
    """ Retorna el nombre de la carpeta en donde se encuentra `filename` """

    return os.path.dirname(filename)


def get_display_name(filename):
    """ Retorna el nombre de `filename` sin la ruta """

    return QFileInfo(filename).fileName()


def get_extension(filename):
    ext = os.path.splitext(filename)[1][1:]
    if ext:
        return ext
    return None


def get_folder_name(directory):
    return os.path.basename(os.path.normpath(directory))