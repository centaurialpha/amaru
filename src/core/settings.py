# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)


import sys

from PyQt5.QtGui import QFont
#from PyQt5.QtCore import QSettings

#from src import SETTINGS_FILE

# Determinate the OS
LINUX, WINDOWS = False, False

if sys.platform == 'linux':
    LINUX = True
else:
    WINDOWS = True

SUPPORTED_FILES = {
    'py': 'python',
    'rb': 'ruby',
    'pl': 'perl'
}

PROGRAM_ARGS = {
    'python': ['-u'],
    'ruby': [],
    'perl': []
}


class Settings:
    # Toolbar settings
    # Visibility
    SHOW_TOOLBAR = False

    # Items
    TOOLBAR = [
        'new',
        'open',
        'save',
        'save_as',
        '',  # Es un separador
        'undo',
        'redo',
        'cut',
        'copy',
        'paste'
    ]

    # Position
    # 0 = None
    # 1 = Left
    # 2 = Right
    # 4 = Top (Default)
    # 8 = Bottom
    TOOLBAR_POSITION = 4

    # Editor options
    MINIMAP = False

    INDENTATION_WIDTH = 4

    WORD_WRAP = False

    SHOW_TABS_AND_SPACES = True

    MARGIN_LINE = True

    MARGIN_LINE_WIDTH = 79

    SHOW_LINE_NUMBERS = True

    font_family = 'Monospace' if LINUX else 'Courier'
    FONT = QFont(font_family)

    CURSOR_WIDTH = 2

    STYLE = ''

    # Files from last session
    FILES = []


def read():
    """ Read all settings from the INI file """

    pass
    #qsettings = QSettings(SETTINGS_FILE, QSettings.IniFormat)
    #print(q)


#def save(key, value):
    #qsettings = QSettings(SETTINGS_FILE, QSettings.IniFormat)
    #qsettings.setValue(key, value, )
