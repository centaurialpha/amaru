# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)


from PyQt5.QtGui import QKeySequence
from PyQt5.QtCore import Qt


# Atajos de teclado por defecto
KEYMAP = {
    'new-file': QKeySequence.New,
    'open-file': QKeySequence.Open,
    'save': QKeySequence.Save,
    'save-as': QKeySequence.SaveAs,
    'close': QKeySequence.Close,
    'exit': QKeySequence.Quit,
    'show-toolbar': QKeySequence(Qt.Key_F7),
    'split-v': QKeySequence(Qt.Key_F10),
    'split-h': QKeySequence(Qt.Key_F9),
    'zoom-in': QKeySequence.ZoomIn,
    'zoom-out': QKeySequence.ZoomOut
}

CUSTOM_KEYMAP = {}
