# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)


class Plugin(object):
    """ Base class for all plugins """

    def __init__(self):
        class_ = self.__class__
        print("Initializing plugin '{0}.{1}'...".format(class_.__module__,
                                                         class_.__name__))

    def start(self):
        raise NotImplemented

    def stop(self):
        raise NotImplemented