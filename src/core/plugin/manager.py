# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

import os

from PyQt5.QtCore import QObject


class PluginManager(QObject):

    def __init__(self, path=None):
        super(PluginManager, self).__init__()
        self.path = path
        self.__plugins = []

    def discover(self):
        files = os.listdir(self.path)
        for f in files:
            ext = os.path.splitext(f)[-1]
            if ext == '.py':
                self.__plugins.append(f)
        print(self.__plugins)