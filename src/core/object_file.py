# -*- coding: utf-8 -*-
# Amaru - Code editor for scripting programmers
#
# This file is part of Amaru
# Copyright 2016 - Gabriel Acosta <acostadariogabriel at gmail>
# License: GPLv3 (see http://www.gnu.org/licenses/gpl.html)

import os

from PyQt5.QtCore import (
    QFileInfo,
    QIODevice,
    QFile,
    QTextStream,
    QFileSystemWatcher,
    QObject,
    pyqtSignal
)


class AmaruIOError(Exception):
    """ IO Exception """


class File(QObject):
    """
    Ésta clase representa un objeto archivo.

    Un objeto archivo tiene propiedades tales como nombre de archivo (filename),
    un nombre sin el path (display name), esto para ser mostrado en pestañas
    por ejemplo, y funciones típicas como leer y escribir, además posee
    propiedades y funciones para el seguimiento del mismo.
    """

    fileChange = pyqtSignal()

    def __init__(self, filename=''):
        QObject.__init__(self)
        self._system_watcher = None
        self.__modification_time = None
        self.__is_new = True

        if filename:
            self.__is_new = False
        else:
            filename = 'Untitled'

        self.__filename = filename

    @property
    def display_name(self):
        """ Returns the name of file without path """

        return QFileInfo(self.filename).fileName()

    @property
    def is_new(self):
        """ Returns True if file is new, False otherwise """

        return self.__is_new

    @property
    def filename(self):
        return self.__filename

    @filename.setter
    def filename(self, filename):
        self.__filename = filename

    def read(self):
        file_ = QFile(self.filename)
        flags = QIODevice.ReadOnly | QIODevice.Text
        if not file_.open(flags):
            raise AmaruIOError(file_.errorString())

        stream = QTextStream(file_)
        content = stream.readAll()
        file_.close()
        return content

    def write(self, content, path=None):
        if self.is_new:
            self.__filename = path
            self.__is_new = False

        file_ = QFile(self.filename)
        flags = QIODevice.WriteOnly | QIODevice.Truncate
        if not file_.open(flags):
            raise AmaruIOError(file_.errorString())

        if not self.is_new:
            self.start_watching()

        # Write the file
        outfile = QTextStream(file_)
        outfile << content

    def start_watching(self):
        if self._system_watcher is None:
            self._system_watcher = QFileSystemWatcher(self)
            self._system_watcher.fileChanged['QString'].connect(
                self.__on_file_changed)

        self._system_watcher.addPath(self.__filename)
        self.__modification_time = os.path.getmtime(self.__filename)

    def __on_file_changed(self, path):
        current_modification_time = os.path.getmtime(self.__filename)
        if current_modification_time != self.__modification_time:
            self.__modification_time = current_modification_time
            self.fileChange.emit()
